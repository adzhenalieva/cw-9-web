import axios from '../axios-contacts';

export const CONTACT_REQUEST = 'CONTACT_REQUEST';
export const CONTACT_SUCCESS = 'CONTACT_SUCCESS';
export const CONTACT_FAILURE = 'CONTACT_FAILURE';

export const CLOSE_MODAL = "CLOSE_MODAL";
export const CONTACT_BY_ID = "CONTACT_BY_ID";


export const CONTACT_EDIT_SUCCESS = 'CONTACT_EDIT_SUCCESS';
export const CONTACT_EDITED = 'CONTACT_EDITED';
export const CONTACT_DELETED = 'CONTACT_DELETED';

export const contactRequest = () => ({type: CONTACT_REQUEST});
export const contactSuccess = response => ({type: CONTACT_SUCCESS, response});
export const contactFailure = error => ({type: CONTACT_FAILURE, error});


export const contactEditSuccess = response => ({type: CONTACT_EDIT_SUCCESS, response});
export const contactEdited = () => ({type: CONTACT_EDITED});


export const fetchContacts = () => {
    return dispatch => {
        dispatch(contactRequest());
        axios.get('/contacts.json').then(response => {
            const contacts = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });
            dispatch(contactSuccess(contacts));
        }, error => {
            dispatch(contactFailure(error));
        });
    }
};

export const getContactByID = id => ({type: CONTACT_BY_ID, id});
export const closeModal = () => ({type: CLOSE_MODAL});

export const getDataForEdit = (id) => {
    return dispatch => {
        axios.get('contacts/' + id + '.json').then(response => {
            dispatch(contactEditSuccess(response.data))
        }, error => {
            dispatch(contactFailure());
        });
    };
};
export const sendDataForEdit = (id, history, contact) => {
    return dispatch => {
        axios.put('contacts/' + id + '.json', contact).then(() => {
            dispatch(contactEdited());
            history.push('/');
        }, error => {
            dispatch(contactFailure());
        });
    };
};


export const contactPost = (history, contact) => {
    axios.post('contacts.json', contact).then(() => {
        history.replace('/');
    });
};


export const contactDelete = (id) => {
    return dispatch => {
        axios.delete('/contacts/' + id + '.json').then(() => {
            dispatch({type: CONTACT_DELETED});
            dispatch(fetchContacts());
        })
    };
};