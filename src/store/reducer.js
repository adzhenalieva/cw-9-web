import {
    CLOSE_MODAL,
    CONTACT_BY_ID,
    CONTACT_DELETED, CONTACT_EDIT_SUCCESS,
    CONTACT_EDITED,
    CONTACT_FAILURE,
    CONTACT_REQUEST,
    CONTACT_SUCCESS
} from "./action";


const initialState = {
    contacts: [],
    loading: true,
    contactById: null,
    error: null,
    show: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CONTACT_SUCCESS:
            return {
                ...state,
                contacts: action.response,
                loading: false
            };
        case CONTACT_REQUEST:
            return {
                ...state, loading: true
            };
        case CONTACT_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case CONTACT_BY_ID:
            return {
                ...state,
                contactById: state.contacts[action.id],
                show: true
            };
        case CLOSE_MODAL:
            return {
                ...state,
                contactById: null,
                show: false
            };
        case CONTACT_DELETED:
            return {
                ...state,
                contactById: null,
                show: false,
                error: action.error
            };
        case CONTACT_EDITED:
            return {
                ...state,
                contactById: null,
                show: false,
                error: action.error
            };
        case CONTACT_EDIT_SUCCESS:
            return {
                ...state,
                contactById: action.response, loading: false
            };
        default:
            return state;
    }
};
export default reducer;