import React from 'react';
import './FullContact.css';

const FullContact = props => {
    return (
        <div className="FullContact">
            <button className="ModalButtonDel" onClick={props.closeModal}>X</button>
            <img className='Image' src={props.photo} alt="contact"/>
            <p> {props.name} </p>
            <p>{props.phone}</p>
            <p>{props.email}</p>
            <button className="ModalButton" onClick={props.edit}>Edit</button>
            <button className="ModalButton" onClick={props.delete}>Delete</button>
        </div>
    );
};

export default FullContact;