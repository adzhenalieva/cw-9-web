import React, {Component} from 'react';
import {connect} from "react-redux";

import Spinner from "../../components/UI/Spinner/Spinner";

import './ContactEdit.css';
import ContactForm from "../ContactForm/ContactForm";
import {getDataForEdit, sendDataForEdit} from "../../store/action";


class ContactEdit extends Component {

    componentDidMount() {
        this.props.getDataForEdit(this.props.match.params.id);
    }

    edit = dish => {
        this.props.edit(this.props.match.params.id, this.props.history, dish)
    };

    render() {
        let contact = <ContactForm onSubmit={this.edit}
                                   contact={this.props.contact}/>;
        if (!this.props.contact) {
            contact = <Spinner/>
        }
        return (
            <div className="ContactEdit">
                <h2>Edit contact</h2>
                {contact}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        contact: state.contactById
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getDataForEdit: id => dispatch(getDataForEdit(id)),
        edit: (id, history, contact) => dispatch(sendDataForEdit(id, history, contact))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactEdit);
