import React, {Component} from 'react';
import {connect} from "react-redux";

import ContactForm from "../ContactForm/ContactForm";
import {contactPost} from "../../store/action";

import './ContactAdd.css';




class ContactAdd extends Component {
    add = contact => {
        this.props.dishPost(this.props.history, contact);
    };

    render() {
        return (
            <div className="ContactAdd">
                <h1>Add new contact</h1>
                <ContactForm onSubmit={this.add}/>
            </div>
        );
    }
}
const mapDispatchToProps = () => {
    return {
        dishPost: (history, contact) => contactPost(history, contact)
    }
};

export default connect(mapDispatchToProps)(ContactAdd);