import React, {Component} from 'react';
import './ContactForm.css';

class ContactForm extends Component {

    constructor(props) {
        super(props);
        if (props.contact) {
            this.state = {...props.contact};
        } else {
            this.state = {
                name: '',
                phone: '',
                email: '',
                photo: '',
            };
        }
    }

    valueChanged = event => {
        let name = event.target.name;
        this.setState({[name]: event.target.value})

    };


    submit = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        let src = null;
        if(this.state.photo === ''){
            src = 'http://www.art-teachers.ru/image/no.jpg'
        }
        else{src = this.state.photo}

        return (
            <form className="ContactForm" onSubmit={this.submit}>
                <input name="name"
                          onChange={this.valueChanged}
                       placeholder={"Name"}
                          value={this.state.name}
                />
                <input type="text" name="phone"
                       placeholder={"Phone"}
                       onChange={this.valueChanged}
                       value={this.state.phone}
                />
                <input type="text" name="email"
                       placeholder={"E-mail"}
                       onChange={this.valueChanged}
                       value={this.state.email}
                />
                <input type="text" name="photo"
                       placeholder={"Photo"}
                       onChange={this.valueChanged}
                       value={this.state.photo}
                />
                <img className="Preview" src={src} alt="preview"/>
                <button type="submit">Save</button>
            </form>
        );
    }
}

export default ContactForm;