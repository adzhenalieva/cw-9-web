import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

import './MainNav.css';


class Nav extends Component {

    render() {
        return (
            <nav className="MainNav">
                <h3 className="Logo">Phone Book</h3>
                <NavLink className="NavLink"   activeClassName="Active" exact={true} to="/">Contacts</NavLink>
                <NavLink className="NavLink"  activeClassName="Active" to="/contacts/add">Add new contact</NavLink>
            </nav>
        );
    }
}

export default Nav;