import React, {Component} from 'react';
import {connect} from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";
import Modal from "../../components/UI/Modal/Modal";
import {closeModal, contactDelete, fetchContacts, getContactByID} from "../../store/action";

import './PhoneBook.css';
import FullContact from "../../components/FullContact/FullContact";


class PhoneBook extends Component {

    componentDidMount() {
        this.props.fetchContacts();
    }

    deleteContact = (id) => {
        this.props.contactDelete(id);
    };

    showModal = id => {
        this.props.getContactByID(id);

    };
    goToEdit = (id) => {
        this.props.history.push({
            pathname: '/contacts/' + id + '/edit'
        })
    };

    render() {
        let contacts = null;
        if (this.props.contacts) {
            contacts = this.props.contacts.map((contact, index) => (
                <div className="Contact" key={contact.id}>
                    <img className='Image' src={contact.photo} alt="contact"/>
                    <p> {contact.name} </p>
                    <button className="ShowMore" onClick={() => this.showModal(index)}>Snow more</button>
                </div>
            ))
        }

        return (
            this.props.loading ?
                <Spinner/> :
                <div className="Contacts">
                    {contacts}
                    <Modal show={this.props.show}
                           close={this.props.closeModal}>
                        {this.props.contactById ?
                            <FullContact
                                closeModal={this.props.closeModal}
                                photo={this.props.contactById.photo}
                                name={this.props.contactById.name}
                                phone={this.props.contactById.phone}
                                email={this.props.contactById.email}
                                edit={() => this.goToEdit(this.props.contactById.id)}
                                delete={() => this.deleteContact(this.props.contactById.id)}
                            />
                            : null
                        }
                    </Modal>
                </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
        loading: state.loading,
        contactById: state.contactById,
        show: state.show
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchContacts: () => dispatch(fetchContacts()),
        getContactByID: id => dispatch(getContactByID(id)),
        closeModal: () => dispatch(closeModal()),
        contactDelete: id => dispatch(contactDelete(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PhoneBook);