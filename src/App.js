import React, {Component} from 'react';
import {Switch, Route} from "react-router-dom";

import PhoneBook from "./containers/PhoneBook/PhoneBook";
import Layout from "./components/Layout/Layout";
import ContactAdd from "./components/ContactAdd/ContactAdd";

import './App.css';
import ContactEdit from "./components/ContactEdit/ContactEdit";


class App extends Component {
    render() {
        return (
            <div className="App">
                <Layout>
                    <Switch>
                        <Route path="/" exact component={PhoneBook}/>
                        <Route path="/contacts/add" exact component={ContactAdd}/>
                        <Route path="/contacts/:id/edit" component={ContactEdit}/>
                    </Switch>
                </Layout>
            </div>
        );
    }
}

export default App;
